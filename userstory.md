# Feature: Agendar Consulta

```
Secanario: Agendar cinsulta para hoje
given tenho o browser aberto
AND estou na pagina da app

WHEN carrego no botao "make appoinment"
AND introduzo o user jonh doe
AND instroduzo a senha do user jonh doe
AND carrego no botao login
AND escolho a data atual 
AND carrego no boao book appointment
THEN verifico a label " appoint confimation"
```

# Feature: Aceder ao Perfil

```
Scenario: Aceder ao perfil e fazer logout
Given: Tenho browser aberto
AND estou na pagina da app

WHEN abro a side bar
AND carrego no botao login
AND introduzo o user jonh doe
AND introduzo a senha do user jonh doe
AND carrego no botao login
AND abro a side bar 
AND carrego no botao perfil
AND carrego no botao logout 
THEN verifico a label " h3 we care about your health"
```

# Feature: Aceder ao Historico

```
Scenario: Aceder ao perfil e fazer logout
Given: Tenho browser aberto
AND estou na pagina da app

WHEN abro a side bar
AND carrego no botao login
AND introduzo o user jonh doe
AND introduzo a senha do user jonh doe
AND carrego no botao login
AND abro a side bar 
AND carrego no botao historico
AND carrego no botao logout 
THEN verifico a label " h3 we care about your health"
```